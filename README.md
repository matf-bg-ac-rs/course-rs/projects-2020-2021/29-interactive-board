# Project 29-Interactive-Board

**[English](#summary)**  
**[Srpski](#opis)**

## Summary

Interactive board is application with functionality of writing/drawing, erasing, changing pen width and color. Also you can save what you made or
open existing picture and edit it. There are more functionalities inside the application and you can read more about them in [application controls](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/29-interactive-board/-/wikis/Application%20controls). 

Interactive board is application for lightweight drawing and writing. It's made to be simple and easy to use, but yet powerful enough for daily usage.

## Usage instructions

All about ways to use the application can be found in [how to use](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/29-interactive-board/-/wikis/How%20To%20Use).

## Developers

- [Lazar Perisic, 480/2018](https://gitlab.com/bambalic)

## Demo

You can see demo [here](https://youtu.be/ljRI7zG-FUU). 

## Opis
Interaktivna tabla sa mogucnoscu pisanja, brisanja, menjanja debljine linije, boje olovke/markera kao i cuvanja tog fajla.
Postoji jos funkcionalnosti o kojima mozete saznati [ovde](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/29-interactive-board/-/wikis/Application%20controls) *(samo na engleskom)*.
Aplikacija je pre svega laka i intuitivna za koriscenje.

## Pokretanje

Sve u vezi kako mozete pokrenuti aplikaciju mozete videti [ovde](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/29-interactive-board/-/wikis/How%20To%20Use)
*(samo na engleskom)*.

## Developeri

- [Lazar Perisic, 480/2018](https://gitlab.com/bambalic)