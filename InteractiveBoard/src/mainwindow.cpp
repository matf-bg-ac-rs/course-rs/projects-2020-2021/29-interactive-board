#include <QtWidgets>
#include <QMessageBox>
#include <QToolBar>
#include "ui_mainwindow.h"
#include "headers/background.h"
#include "headers/mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {

  ui->setupUi(this);

  // setting undo and redo disabled so they won't access empty stacks
  ui->actionUndo->setEnabled(false);
  ui->actionRedo->setEnabled(false);

  auto *spacerMenuBar = new QWidget();
  spacerMenuBar->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);

  ui->toolBarMenu->addAction(ui->actionOpen);
  ui->toolBarMenu->addAction(ui->actionSave);
  ui->toolBarMenu->addAction(ui->actionClose);
  ui->toolBarMenu->addAction(ui->actionUndo);
  ui->toolBarMenu->addAction(ui->actionRedo);
  ui->toolBarMenu->addWidget(spacerMenuBar);

  // separate clear action from other widgets so it doesnt get clicked
  // accidentally even though it can be undo-ed
  ui->toolBarWidgets->addAction(ui->actionPen);
  ui->toolBarWidgets->addAction(ui->actionEraser);
  ui->toolBarWidgets->addAction(ui->actionHighlighter);
  ui->toolBarWidgets->addAction(ui->actionColor_Palette);

  // Spacer
  auto *spacerWidgetBar = new QWidget();
  spacerWidgetBar->setSizePolicy(
      QSizePolicy::Expanding, QSizePolicy::Preferred);

  ui->toolBarWidgets->addWidget(spacerWidgetBar);

  ui->toolBarWidgets->addAction(ui->actionClear);

  // make toolbars in 2 rows
  QToolBar *first = ui->toolBarWidgets;
  insertToolBarBreak(first);

  // fullscreen
  QWidget::showMaximized();

  path = QDir::home().path() + "/Pictures";

  board = new Background;

  scrollBarPenWidth = new QScrollBar(Qt::Orientation(Qt::Horizontal));
  scrollBarEraserWidth = new QScrollBar(Qt::Orientation(Qt::Horizontal));
  scrollBarHighlighterTransparency =
      new QScrollBar(Qt::Orientation(Qt::Horizontal));
  ;

  scrollBarPenWidth->setValue(board->getPenWidth());
  scrollBarEraserWidth->setValue(board->getEraserWidth());
  scrollBarHighlighterTransparency->setValue(
      board->getHighlighterTransparentLevel());

  auto *centralWidget = new QWidget;

  auto *layoutH1 = new QHBoxLayout;
  auto *layoutH2 = new QHBoxLayout;
  auto *layoutH3 = new QHBoxLayout;

  labelPenWidth = new QLabel(tr("Pen width"));
  labelPenWidth->setFixedWidth(200);
  labelEraserWidth = new QLabel(tr("Eraser width"));
  labelEraserWidth->setFixedWidth(200);
  labelHighlighterTransparency =
      new QLabel(tr("Highlighter transparent level"));
  labelHighlighterTransparency->setFixedWidth(200);

  layoutH1->addWidget(labelPenWidth);
  layoutH1->addWidget(scrollBarPenWidth);

  layoutH2->addWidget(labelEraserWidth);
  layoutH2->addWidget(scrollBarEraserWidth);

  layoutH3->addWidget(labelHighlighterTransparency);
  layoutH3->addWidget(scrollBarHighlighterTransparency);

  auto *layout = new QVBoxLayout;
  //  layout->addWidget(scrollBarPenWidth);
  //  layout->addWidget(scrollBarEraserWidth);
  //  layout->addWidget(scrollBarHighlighterTransparency);

  layout->addLayout(layoutH1);
  layout->addLayout(layoutH2);
  layout->addLayout(layoutH3);
  layout->addWidget(board);

  centralWidget->setLayout(layout);

  setCentralWidget(centralWidget);

  // setting board as central widget
  //  setCentralWidget(board);

  QObject::connect(
      this, &MainWindow::needToSave, board, &Background::saveImage);

  QObject::connect(
      this, &MainWindow::needToOpenImg, board, &Background::openBackground);

  QObject::connect(
      this, &MainWindow::colorChanged, board, &Background::setPenColor);

  QObject::connect(this, &MainWindow::undo, board, &Background::undoFunc);

  QObject::connect(this, &MainWindow::redo, board, &Background::redoFunc);

  QObject::connect(
      board, &Background::activatedUndo, this, &MainWindow::activateUndo);

  QObject::connect(scrollBarPenWidth, &QAbstractSlider::valueChanged, board,
      &Background::setPenWidth);

  QObject::connect(scrollBarEraserWidth, &QAbstractSlider::valueChanged, board,
      &Background::setEraserWidth);

  QObject::connect(scrollBarHighlighterTransparency,
      &QAbstractSlider::valueChanged, board,
      &Background::setHighlighterTransparentLevel);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::on_actionClose_triggered() {
  QMessageBox::StandardButton reply;
  reply = QMessageBox::question(this, tr("Warning"),
      tr("Do you really want to quit?"),
      QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);

  if (reply == QMessageBox::Yes)
    QApplication::quit();
}

void MainWindow::on_actionUndo_triggered() {
  ui->actionRedo->setEnabled(true);

  emit undo();

  if (board->undoSlides.empty())
    ui->actionUndo->setEnabled(false);
}

void MainWindow::activateUndo() {
  ui->actionUndo->setEnabled(true);
  ui->actionRedo->setEnabled(false);
}

void MainWindow::on_actionRedo_triggered() {

  ui->actionUndo->setEnabled(true);

  emit redo();

  if (board->redoSlides.empty())
    ui->actionRedo->setEnabled(false);
}

// when exiting app on X give same prompt as exiting on widget
void MainWindow::closeEvent(QCloseEvent *event) {
  emit MainWindow::on_actionClose_triggered();
  event->ignore();
}

void MainWindow::on_actionOpen_triggered() {
  QString fileName = QFileDialog::getOpenFileName(
      this, tr("Set picture as background / Open picture"), path);

  if (!fileName.isEmpty() && !fileName.isNull()) {
    auto lastIndexOfForwardSlashInFilePath = fileName.lastIndexOf("/");

    MainWindow::path =
        fileName.chopped(fileName.size() - lastIndexOfForwardSlashInFilePath);

    board->openBackground(fileName);
  }
}

void MainWindow::on_actionSave_triggered() {
  QByteArray fileFormat = "png";

  QString initialPath = MainWindow::path + "/untitled." + fileFormat;

  QString filename = QFileDialog::getSaveFileName(this, tr("Save"), initialPath,
      tr("%1 Files (*.%2);;")
          .arg(QString::fromLatin1(fileFormat))
          .arg(QString::fromLatin1(fileFormat)));

  if (filename.isEmpty())
    return;

  emit needToSave(filename, fileFormat.constData());
}

void MainWindow::on_actionColor_Palette_triggered() { emit colorChanged(); }

void MainWindow::on_actionPen_triggered() { board->setWidget(Background::Pen); }

void MainWindow::on_actionEraser_triggered() {
  board->setWidget(Background::Eraser);
}

void MainWindow::on_actionClear_triggered() { board->clearBackground(); }

void MainWindow::on_actionHighlighter_triggered() {
  board->setWidget(Background::Highlighter);
}

void MainWindow::on_actionAbout_triggered() {
  QMessageBox::information(this, tr("About"),
      tr("Interactive board app\n\n"
         "Interactive board is application for lightweight drawing and "
         "writing. Its made to be simple and easy to use, but yet powerful "
         "enough for daily usage.\n\n"
         "Made by Laki"));
}
