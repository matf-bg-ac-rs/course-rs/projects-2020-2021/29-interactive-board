#include "headers/background.h"

Background::Background(QWidget *parent) : QWidget(parent) {
  // puts the widget in top left corner
  setAttribute(Qt::WA_StaticContents);

  // setting background size and color
  background = (QImage(1920, 1080, QImage::Format_RGB32));
  background.fill(QColor(255, 255, 255));

  modified = false;

  penWidth = 10;
  penColor = Qt::black;

  eraserWidth = 10;
  eraserColor = Qt::white;

  drawing = true;
  erasing = false;

  colorInUse = Qt::black;
  widget = Pen;

  undoSlides = std::stack<QImage>{};
  redoSlides = std::stack<QImage>{};

  transparentLevel = 10;
}

Background::~Background() {}

// open "image" to use it as background to write on
bool Background::openBackground(const QString &fileName) {

  QImage backgroundToUse;

  // if something failed leave the function
  if (!backgroundToUse.load(fileName))
    return false;

  background = backgroundToUse;

  modified = false;
  update();
  return true;
}

bool Background::saveImage(const QString &filename, const char *fileFormat) {
  QImage imageToSave = background;

  if (imageToSave.save(filename, fileFormat)) {
    modified = false;
    return true;
  } else {
    return false;
  }
}

void Background::setPenColor() {
  auto answer = QColorDialog::getColor(penColor);
  penColor = answer.isValid() ? answer : penColor;
}

void Background::clearBackground() {
  background.fill(qRgb(255, 255, 255));
  modified = true;
  update();
}

void Background::mousePressEvent(QMouseEvent *event) {
  if (event->button() == Qt::LeftButton) {
    if (widget == Pen || widget == Highlighter) {
      colorInUse = penColor;
      drawing = true;
      erasing = false;

      undoSlides.push(background);
      redoSlides = std::stack<QImage>{};
      emit activatedUndo();
    } else if (widget == Eraser) {
      colorInUse = eraserColor;
      drawing = false;
      erasing = true;

      undoSlides.push(background);
      redoSlides = std::stack<QImage>{};
      emit activatedUndo();
    }
    lastPoint = event->pos();
    //    drawing = true;
  }
}

void Background::mouseMoveEvent(QMouseEvent *event) {
  if ((event->buttons() & Qt::LeftButton) && drawing) {
    pen(event->pos());
  } else if ((event->buttons() & Qt::LeftButton) && erasing) {
    eraser(event->pos());
  }
}

void Background::mouseReleaseEvent(QMouseEvent *event) {
  if (event->button() == Qt::LeftButton && drawing) {
    pen(event->pos());
    drawing = false;
  } else if ((event->buttons() & Qt::LeftButton) && erasing) {
    eraser(event->pos());
    erasing = false;
    //  } else if (colorPicker) {
    //        penColor = background.pixelColor(event->pos());

    //    colorPicker = false;
    //    drawing = true;
  }
}

void Background::paintEvent(QPaintEvent *event) {

  QPainter painter(this);

  // updating only part that needs to be updated
  QRect dirtyRect = event->rect();
  painter.drawImage(dirtyRect, background, dirtyRect);
}

void Background::pen(const QPoint &endPoint) {
  QPainter painter(&background);

  if (widget == Pen) {

    painter.setPen(
        QPen(colorInUse, penWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
  } else if (widget == Highlighter) {

    QColor colorInUseTransparent = colorInUse;
    colorInUseTransparent.setAlpha(transparentLevel);
    painter.setPen(QPen(colorInUseTransparent, penWidth, Qt::SolidLine,
        Qt::RoundCap, Qt::RoundJoin));
  }

  painter.drawLine(lastPoint, endPoint);

  modified = true;

  int rad = (penWidth / 1) + 2;

  update(
      QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));

  lastPoint = endPoint;
}

void Background::eraser(const QPoint &endPoint) {

  QPainter painter(&background);

  painter.setPen(QPen(
      colorInUse, eraserWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

  painter.drawLine(lastPoint, endPoint);

  modified = true;

  int rad = (eraserWidth / 1) + 2;

  update(
      QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));

  lastPoint = endPoint;
}

// void Background::setColorPicker() {
//  colorPicker = true;
//  drawing = false;
//}

void Background::undoFunc() {
  redoSlides.push(background);
  background = undoSlides.top();
  undoSlides.pop();
  setMinimumSize(
      QSize(background.size().rwidth() * 1, background.size().rheight() * 1));

  update();
}

void Background::redoFunc() {
  undoSlides.push(background);
  background = redoSlides.top();
  redoSlides.pop();
  setMinimumSize(
      QSize(background.size().rwidth() * 1, background.size().rheight() * 1));

  update();
}
