#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QImage>
#include <QSize>
#include <QCloseEvent>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QString>
#include <QFileDialog>
#include <QDialog>
#include <QList>
#include <QColorDialog>
#include <QToolBar>
#include <QScrollBar>
#include "qpainter.h"
#include "background.h"

class Background;

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

  public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

  signals:
  void colorChanged();
  void widthChanged();
  void needToSave(QString filename, const char *fileFormat);
  void needToOpenImg(const QString &filename);

  void undo();
  void redo();

  public slots:

  void activateUndo();

  private slots:

  void on_actionClose_triggered();
  void on_actionOpen_triggered();
  void on_actionSave_triggered();
  void closeEvent(QCloseEvent *) override;

  void on_actionUndo_triggered();
  void on_actionRedo_triggered();

  void on_actionPen_triggered();
  void on_actionEraser_triggered();
  void on_actionHighlighter_triggered();
  void on_actionColor_Palette_triggered();

  void on_actionClear_triggered();

  void on_actionAbout_triggered();

  private:
  Ui::MainWindow *ui;
  QString path;
  Background *board;

  QScrollBar *scrollBarPenWidth;
  QScrollBar *scrollBarEraserWidth;
  QScrollBar *scrollBarHighlighterTransparency;

  QLabel *labelPenWidth;
  QLabel *labelEraserWidth;
  QLabel *labelHighlighterTransparency;

  protected:
};
#endif // MAINWINDOW_H
