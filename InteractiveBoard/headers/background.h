#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <QObject>
#include <stack>

#include <QMainWindow>
#include <QImage>
#include <QColor>
#include <QWidget>
#include <QColorDialog>
#include <QInputDialog>
#include "mainwindow.h"

class Background : public QWidget {

  Q_OBJECT

  public:
  Background(QWidget *parent = 0);
  ~Background();

  enum Widgets { nothing, Pen, Eraser, Highlighter };

  bool saveImage(const QString &filename, const char *fileFormat);

  bool openBackground(const QString &fileName);
  QImage getBackground() const { return background; }

  QColor getPenColor() const { return penColor; }
  int getPenWidth() { return penWidth; }
  int getEraserWidth() { return eraserWidth; }
  int getHighlighterTransparentLevel() { return transparentLevel; }

  bool isModified() const { return modified; }
  //  void setColorPicker();

  void setWidget(const Widgets &newWidget) { widget = newWidget; }
  Widgets getWidget() { return widget; }

  std::stack<QImage> undoSlides;
  std::stack<QImage> redoSlides;

  signals:
  void activatedUndo();

  public slots:

  void setPenWidth(int newWidth) { penWidth = newWidth; }
  void setPenColor();
  void setEraserWidth(int newWidth) { eraserWidth = newWidth; }
  void setHighlighterTransparentLevel(int newTransparentLevel) {
    transparentLevel = newTransparentLevel;
  }

  void clearBackground();

  void undoFunc();
  void redoFunc();

  protected:
  void mousePressEvent(QMouseEvent *event) override;
  void mouseMoveEvent(QMouseEvent *event) override;
  void mouseReleaseEvent(QMouseEvent *event) override;
  void paintEvent(QPaintEvent *event) override;

  private:
  void pen(const QPoint &endPoint);
  void eraser(const QPoint &endPoint);

  bool modified;

  int penWidth;
  QColor penColor;

  int eraserWidth;
  QColor eraserColor;

  QColor colorInUse;

  QImage background;
  QPoint lastPoint;

  bool drawing;
  bool erasing;

  Widgets widget;

  int transparentLevel;
};

#endif // BACKGROUND_H
